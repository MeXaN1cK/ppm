import javafx.application.Application
import tornadofx.App
import UI.PPMView

fun main(args: Array<String>) {
    Application.launch(PPM::class.java, *args)
}

class PPM: App (PPMView::class)