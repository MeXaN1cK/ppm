package Converter

import java.awt.Color
import java.awt.image.BufferedImage
import javafx.scene.image.*

object Converter {
    class BasicBitmapStorage(width: Int, height: Int) {
        private val image = BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR)
        fun fill(c: Color) {
            val g = image.graphics
            g.color = c
            g.fillRect(0, 0, image.width, image.height)
        }
        fun setPixel(x: Int, y: Int, c: Color) = image.setRGB(x, y, c.rgb)
        fun getPixel(x: Int, y: Int) = Color(image.getRGB(x, y))
    }
    fun convert(image: Image): ByteArray{
        val reader = image.pixelReader
        val width = image.width.toInt()
        val height= image.height.toInt()

        val bbs = BasicBitmapStorage(width, height)
        for (y in 0 until height) {
            for (x in 0 until width) {
                val col = reader.getColor(x,y)
                val c = Color((col.red * 255).toInt(),(col.green * 255).toInt(),(col.blue * 255).toInt())
                bbs.setPixel(x, y, c)
            }
        }
        val header = "P6\n$width $height\n255\n".toByteArray()
        val result = ByteArray(width * height * 3 + header.size)
        var i = 0
        while (i < header.size) {
            result[i] = header[i]
            i++
        }
        for (y in 0 until height) {
            for (x in 0 until width) {
                val c = bbs.getPixel(x, y)
                result[i] = c.red.toByte()
                result[i + 1] = c.green.toByte()
                result[i + 2] = c.blue.toByte()
                i += 3
            }
        }
        return result
    }
}