package UI

import tornadofx.*
import javafx.scene.image.*
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Pos
import javafx.geometry.Rectangle2D
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import FilePicker
import Converter.Converter
import javafx.scene.control.Alert
import javafx.stage.FileChooser
import java.nio.file.Files

class PPMView : View() {
    private val openFilePicker = FilePicker()
    private val exportFilePicker = FilePicker()
    private val image = SimpleObjectProperty(Image("/motorhead.jpg"))
    private val preview = imageview {
        viewport = Rectangle2D(0.0, 0.0, 160.0, 100.0)
    }

    override val root = vbox {
        style {
            padding = box(10.px)
            spacing = 5.px
        }
        hbox {
            style {
                spacing = 5.px
            }
            button("Open Image...") {
                style {
                    fontWeight = FontWeight.EXTRA_BOLD
                    borderColor += box(
                        top = Color.RED,
                        right = Color.DARKGREEN,
                        left = Color.ORANGE,
                        bottom = Color.PURPLE
                    )
                }
                action {
                    val result = openFilePicker.chooseFile("Select image file",
                        arrayOf(
                            FileChooser.ExtensionFilter("Supported image format",
                            listOf("*.png", "*.jpg", "*.jpeg", "*.bmp"))))
                    if (result.isNotEmpty()) {
                        val file = result.first()
                        image.value = Image("file:${file.absolutePath}")
                    }
                }
            }
            button("Save Image...") {
                style {
                    fontWeight = FontWeight.EXTRA_BOLD
                    borderColor += box(
                        top = Color.RED,
                        right = Color.DARKGREEN,
                        left = Color.ORANGE,
                        bottom = Color.PURPLE
                    )
                }
                action {
                    val files = exportFilePicker.chooseFile("Select file to save",
                        arrayOf(FileChooser.ExtensionFilter("PPM Image",
                            listOf("*.ppm"))), FileChooserMode.Save)
                    if (files.isNotEmpty()) {
                        runAsync {
                            val result = Converter.convert(image.value)
                            Files.write(files.first().toPath(), result)
                        } ui {
                            alert(Alert.AlertType.CONFIRMATION, "Converted successfully")
                        }
                    }
                }
            }
        }
        hbox {
            style {
                padding = box(1.px)
                alignment = Pos.CENTER
                borderWidth += box(1.px)
                borderStyle += BorderStrokeStyle.SOLID
                borderColor += box(Color.DARKGRAY)
            }
            children += preview
        }
    }
    init {
        title = "PPM Image Converter"
        primaryStage.isResizable = false
        preview.imageProperty().bind(image)
    }
}
